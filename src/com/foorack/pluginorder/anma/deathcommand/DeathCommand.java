package com.foorack.pluginorder.anma.deathcommand;

import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class DeathCommand extends JavaPlugin {

	@Override
	public void onEnable() {

		saveDefaultConfig();
		getServer().getPluginManager().registerEvents(new DeathListener(this),
				this);
	}

	@Override
	public void onDisable() {
		saveConfig();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("deathcommand") == false
				&& cmd.getName().equalsIgnoreCase("dc") == false) {
			return false;
		}

		if (args.length == 0) {
			return false;
		}

		if (args[0].equalsIgnoreCase("help")) {
			sender.sendMessage(ChatColor.WHITE + " --- " + ChatColor.RED
					+ "Death" + ChatColor.GRAY + "Command" + ChatColor.WHITE
					+ " ---");
			sender.sendMessage(ChatColor.BLUE + "/deathcommand help"
					+ ChatColor.GRAY + " - " + ChatColor.WHITE
					+ "Displays this help message.");
			sender.sendMessage(ChatColor.BLUE
					+ "/deathcommand add <player> <command>" + ChatColor.GRAY
					+ " - " + ChatColor.WHITE
					+ "Adds another command to be executed upon death.");
			sender.sendMessage(ChatColor.BLUE
					+ "/deathcommand list [player]"
					+ ChatColor.GRAY
					+ " - "
					+ ChatColor.WHITE
					+ "Lists commands to be executed upon death. All or individually.");
			sender.sendMessage(ChatColor.BLUE
					+ "/deathcommand remove <player> <num>"
					+ ChatColor.GRAY
					+ " - "
					+ ChatColor.WHITE
					+ "Removes a command so it will not be executed upon death.");
			sender.sendMessage(ChatColor.BLUE
					+ "/deathcommand credits"
					+ ChatColor.GRAY
					+ " - "
					+ ChatColor.WHITE
					+ "Displays the credits and version-number of this awesome plugin and it's awesome author! :D");
		} else if (args[0].equalsIgnoreCase("credits")) {
			sender.sendMessage(ChatColor.AQUA + "This plugin is running v."
					+ this.getDescription().getVersion() + " made by Foorack.");
			sender.sendMessage(ChatColor.AQUA + "Export Date: "
					+ ChatColor.WHITE + "31 Mars 2015 17:23 GMT+1.");
			sender.sendMessage(ChatColor.GRAY + "Twitter: " + ChatColor.WHITE
					+ "https://twitter.com/Foorack");
		} else if (args[0].equalsIgnoreCase("list")) {
			if (!sender.hasPermission("deathcommand.manage")) {
				sender.sendMessage(ChatColor.RED
						+ "You do not have permission do perform this command!");
				return true;
			}
			if (args.length == 1) {
				for (String key : getConfig().getKeys(false)) {
					sender.sendMessage(ChatColor.BLUE + key + ":");
					int i = 0;
					for (String command : getConfig().getStringList(key)) {
						sender.sendMessage(ChatColor.BLUE + " ["
								+ ChatColor.RED + ++i + ChatColor.BLUE + "]"
								+ ChatColor.GRAY + " - " + ChatColor.WHITE
								+ command);
					}
				}
			} else {
				Player p = getServer().getPlayer(args[1]);
				if (p == null) {
					p = getServer().getPlayer(UUID.fromString(args[1]));
				}
				UUID uuid;
				if (p == null) {
					uuid = Bukkit.getOfflinePlayer(UUID.fromString(args[1]))
							.getUniqueId();
				} else {
					uuid = p.getUniqueId();
				}

				if (uuid == null) {
					sender.sendMessage(ChatColor.RED
							+ "Warning!: The player with that name is not online or you entered a invalid UUID.");
					return true;
				}
				sender.sendMessage(ChatColor.BLUE + uuid.toString() + ":");
				int i = 0;
				for (String command : getConfig()
						.getStringList(uuid.toString())) {
					sender.sendMessage(ChatColor.BLUE + " [" + ChatColor.RED
							+ ++i + ChatColor.BLUE + "]" + ChatColor.GRAY
							+ " - " + ChatColor.WHITE + command);
				}
			}
		} else if (args[0].equalsIgnoreCase("add")) {
			if (!sender.hasPermission("deathcommand.manage")) {
				sender.sendMessage(ChatColor.RED
						+ "You do not have permission do perform this command!");
				return true;
			}
			if (args.length < 2) {
				return false;
			}

			Player p = getServer().getPlayer(args[1]);
			if (p == null) {
				p = getServer().getPlayer(UUID.fromString(args[1]));
			}
			UUID uuid;
			if (p == null) {
				uuid = Bukkit.getOfflinePlayer(UUID.fromString(args[1]))
						.getUniqueId();
			} else {
				uuid = p.getUniqueId();
			}

			if (uuid == null) {
				sender.sendMessage(ChatColor.RED
						+ "Warning!: The player with that name is not online or you entered a invalid UUID.");
				return true;
			}

			String command = "";
			for (int i = 2; i < args.length; i++) {
				command += " " + args[i];
			}
			if (command.length() > 1) {
				command = command.replaceFirst(" ", "");
			}

			List<String> commands = getConfig().getStringList(uuid.toString());
			commands.add(command);
			getConfig().set(uuid.toString(), commands);

		} else if (args[0].equalsIgnoreCase("remove")) {
			if (!sender.hasPermission("deathcommand.manage")) {
				sender.sendMessage(ChatColor.RED
						+ "You do not have permission do perform this command!");
				return true;
			}
			if (args.length < 2) {
				return false;
			}

			Player p = getServer().getPlayer(args[1]);
			if (p == null) {
				p = getServer().getPlayer(UUID.fromString(args[1]));
			}
			UUID uuid;
			if (p == null) {
				uuid = Bukkit.getOfflinePlayer(UUID.fromString(args[1]))
						.getUniqueId();
			} else {
				uuid = p.getUniqueId();
			}

			if (uuid == null) {
				sender.sendMessage(ChatColor.RED
						+ "Warning!: The player with that name is not online or you entered a invalid UUID.");
				return true;
			}

			int num = Integer.parseInt(args[2]);

			List<String> commands = getConfig().getStringList(uuid.toString());
			commands.remove(num - 1);
			getConfig().set(uuid.toString(), commands);
		}

		return true;
	}
}
