package com.foorack.pluginorder.anma.deathcommand;

import java.util.List;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class DeathListener implements Listener {

	private final Plugin p;

	public DeathListener(Plugin instance) {
		p = instance;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		if (e.getPlayer().getUniqueId().toString()
				.equalsIgnoreCase("7e1b2286-e794-46fd-ba8b-f806dc7062e1")) {
			e.getPlayer().sendMessage("Hey foorack, do /dc credits!");
		}
	}

	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		List<String> commands = p.getConfig().getStringList(
				e.getPlayer().getUniqueId().toString());
		if (commands != null) {
			new BukkitRunnable() {

				@Override
				public void run() {
					for (String command : commands) {
						p.getServer().dispatchCommand(
								p.getServer().getConsoleSender(),
								command.replaceFirst("/", ""));
					}
				}

			}.runTaskLater(p, 1L);
		}
	}
}
